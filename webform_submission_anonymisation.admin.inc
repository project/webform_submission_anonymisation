<?php

/**
 * @file
 * Contains the settings page for Webform Submission Anonymisation.
 *
 */

function webform_submission_anonymisation_settings($form, &$form_state) {

  // Retention duration
  $form['retention_duration'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#title' => t('Retention duration'),
    '#attributes' => array(
      'class' => array('container-inline'),
    ),
    'retention_duration_count' => array(
      '#type' => 'textfield',
      '#attributes' => array(
        ' type' => 'number',
        'min' => 0,
        'max' => 1000,
        'step' => 1,
        'size' => 3
      ),
      '#default_value' => variable_get('wsa_retention_duration_count',3)
    ),
    'retention_duration_unit' => array(
      '#type' => 'select',
      '#options' => array(
        'hours' => t('Hours'),
        'days' => t('Days'),
        'years' => t('Years'),
      ),
      '#default_value' => variable_get('wsa_retention_duration_unit','years')
    )
  );

  // Webform fields to anonymize
  $form['webform_forms'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#title' => t('Webforms configuration'),
    '#description' => t('Enable the fields that should be anonymized after retention period is elapsed.')
  );

  $query = db_select('webform', 'w');
  $query->join('node','n','n.nid = w.nid');
  $query->fields('n');
  $webform_nodes = $query->execute()->fetchAllAssoc('nid');
  $webform_nodes = node_load_multiple(array_keys($webform_nodes));

  $webforms_variables = variable_get('wsa_webforms',[]);
  foreach ($webform_nodes as $key => $node) {
    
    $form['webform_forms'][$key] = array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#title' => t('<em>%webform_title</em> form',['%webform_title'=>$node->title])
    );

    $default_value = TRUE; 
    $form['webform_forms'][$key]['webform__' . $key . '__remote_addr'] = array(
      '#type' => 'checkbox',
      '#title' => t('Remote Address (IP)'),
      '#value' => $default_value,
      '#disabled' => true
    );

    $webform_components = $node->webform['components'];
    foreach($webform_components as $component_key => $component){

      if(!in_array($component['form_key'],['actions'])){
      
        // Prepare title
        $title = trim($component['name'],'*');

        // TODO: Set default_value to TRUE when field name contains "name", "nom" or "mail" (only once when installed)
        $default_value = FALSE;
        //$default_value = strpos($field_name,'name') !== false ? TRUE : $default_value;
        //$default_value = strpos($field_name,'nom') !== false ? TRUE : $default_value;
        //$default_value = strpos($field_name,'mail') !== false ? TRUE : $default_value;

        $value = !empty($webforms_variables) ? $webforms_variables[$key][$component_key] : $default_value;
        $form['webform_forms'][$key]['webform__' . $key . '__' . $component_key] = array(
          '#type' => 'checkbox',
          '#title' => $title,
          '#value' => $value
        );

      }
    }

  }

  $form[] = array(
    '#type' => 'submit',
    '#submit' => array('_webform_submission_anonymisation_submit'),
    '#value' => t('Save configuration'),
  );

  return $form; //system_settings_form($form);
}

function _webform_submission_anonymisation_submit($form, &$form_state) {

  $webforms_settings = array();
  foreach($form_state['input'] as $key => $value){
    switch ($key){
      case 'retention_duration_count':
        variable_set('wsa_retention_duration_count',$value);
        break;
      case 'retention_duration_unit':
        variable_set('wsa_retention_duration_unit',$value);
        break;
      default:
        if(strpos($key,'webform__') === 0 && $value){
          $explode = explode('__', $key);
          $webforms_settings[$explode[1]][$explode[2]] = $value;
        }
        break;
    }
  }
  variable_set('wsa_webforms',$webforms_settings);

  _webform_submission_anonymisation_anonymize_webforms();

}
