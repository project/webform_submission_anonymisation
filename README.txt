Description
-----------
The Webform Submission Anonymize module remove personal datas from webform submissions  

Requirements
------------
Drupal 7.x

Installation
------------
1. Copy the entire webform_submission_anonymisation directory the Drupal sites/all/modules directory.

2. Login as an administrator. Enable the module in the "Administer" -> "Modules"

3. (Optional) Edit the settings under "Administer" -> "Configuration" ->
   "Content authoring" -> "Webform settings"

Support
-------
